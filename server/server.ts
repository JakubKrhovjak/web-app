import path from "path";

import express from "express"
import bodyParser from "body-parser";
import cors from "cors";
import session from "express-session"

import { query } from "./mongoClient"
import { basicAuthFilter } from "./filter/basicAuthFilter"

export const rootDir = path.resolve(__dirname)


// // import  { generateToken }  from "./service/jwtService"
// //
// import { FileService } from "./service/fileService"
// import { JwtService } from "./service/jwtService"
//
//
// // let fileService = new FileService(path.resolve(__dirname));
// // let jwtService = new JwtService(fileService);
// // let s = jwtService.generateToken("test");
//
 const app = express();

app.use(bodyParser.json())
app.use(cors({
    origin: "http://localhost:3000",
}))






const sessionOption = {
    secret: "total secret",
    resave: false,
    saveUninitialized: true,
    cookie: {
        httpOnly: true,
        secure: false,
    }
}

app.use(session(sessionOption));

app.use(basicAuthFilter)


app.get('/', async function (req, res) {
    res.send('Hello World');
})


app.post('/sign-in', async (req, res) => {
    const user = await query("users", coll => coll.findOne(req.body));
    return res.send(user);
})

app.post('/new-account', async (req, res) => {
    await query("users", coll => coll.insertOne(req.body))
    return res.send(req.body);
})


app.get('/basic', async (req, res) => {
    res.send("ok")
})


var server = app.listen(8081, () => {

    // var host = server.address().address
    // var port = server.address().port

    console.log("Server started! ")
})
