import  jwt  from "jsonwebtoken"
import { FileService }  from "./fileService"

export class JwtService {

    fileService = new FileService();

    generateToken(username): string {
        const privateKey = this.fileService.readFile( "/config/private.key");
        const publicKey = this.fileService.readFile( "/config/public.key");

        var signOptions = {
            issuer:  "web-app",
            subject: username,
            audience:  "http://localhost:3000",
            expiresIn:  "15m",
            algorithm:  "RS256"
        };
        let sign = jwt.sign({ter: "ter"}, privateKey, signOptions);
        let decode = jwt.decode(sign);
        return sign;

    }

}


