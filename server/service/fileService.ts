import fs from "fs";
import { rootDir } from "../server"

export class FileService {

    readFile(path: string): string {
        return fs.readFileSync(rootDir + path,  {encoding: "utf8"});
    }
}

